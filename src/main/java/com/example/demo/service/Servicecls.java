package com.example.demo.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.Entitycls;
import com.example.demo.helper.Helper;
import com.example.demo.repository.Repositorycls;


@Service
@Component
public class Servicecls {
  @Autowired
  Repositorycls repo;

  public void save(MultipartFile file) {
    try {
      List<Entitycls> tutorials = Helper.csvToTutorials(file.getInputStream());
      repo.saveAll(tutorials);
    } catch (IOException e) {
      throw new RuntimeException("fail to store csv data: " + e.getMessage());
    }
  }

  public List<Entitycls> getAllTutorials() {
    return repo.findAll();
  }
}
