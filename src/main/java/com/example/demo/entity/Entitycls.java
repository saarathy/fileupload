package com.example.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "BigTutorials")
public class Entitycls {


	  @Id
	  @Column(name = "id")
	  private int id;

	  @Column(name = "name")
	  private String name;

	  @Column(name = "description")
	  private String description;

	  @Column(name = "year")
	  private int year;

	  public Entitycls() {

	  }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public String toString() {
		return "Entitycls [id=" + id + ", name=" + name + ", description=" + description + ", year=" + year + "]";
	}

	public Entitycls(int id, String name, String description, int year) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.year = year;
	}
	
	
}
